const mongoose = require('mongoose');

const stopSchema =  new mongoose.Schema({
  agency_key: {
    type: String,
    required: true,
    index: true
  },
  stop_id: {
    type: String,
    required: true,
    index: true,
    unique: true
  },
  stop_code: {
    type: String,
    index: true
  },
  stop_name: {
    type: String,
    required: true
  },
  stop_desc: String,
  stop_lat: {
    type: Number,
    required: true,
    min: -90,
    max: 90
  },
  stop_lon: {
    type: Number,
    required: true,
    min: -180,
    max: 180
  },
  loc: {
    type: [Number],
    index: '2dsphere'
  },
  zone_id: String,
  stop_url: String,
  location_type: {
    type: Number,
    min: 0,
    max: 1
  },
  parent_station: String,
  stop_timezone: String,
  wheelchair_boarding: {
    type: Number,
    min: 0,
    max: 2
  }
});

stopSchema.virtual('trip', {
  ref: 'StopTime', // The model to use
  localField: 'stop_id', // Find people where `localField`
  foreignField: 'stop_id', // is equal to `foreignField`
});

module.exports = mongoose.model('Stop',stopSchema);
