const Agency = require('../models/gtfs/agency');
const CalendarDate = require('../models/gtfs/calendar-date');
const Calendar = require('../models/gtfs/calendar');
const Route = require('../models/gtfs/route');
const StopTime = require('../models/gtfs/stop-time');
const Stop = require('../models/gtfs/stop');
const Trip = require('../models/gtfs/trip');


module.exports = [
  {
    filenameBase: 'agency',
    model: Agency
  }, {
    filenameBase: 'calendar_dates',
    model: CalendarDate
  }, {
    filenameBase: 'calendar',
    model: Calendar
  }, {
    filenameBase: 'routes',
    model: Route
  }, {
    filenameBase: 'stop_times',
    model: StopTime
  }, {
    filenameBase: 'stops',
    model: Stop
  }, {
    filenameBase: 'trips',
    model: Trip
  }
];
