// GTFS import script
const importGTFS = require('./import');

// Standard GTFS Filenames
const {getAgencies} = require('./gtfs/agencies');
const {getCalendars} = require('./gtfs/calendars');
const {getCalendarDates} = require('./gtfs/calendar-dates');
const {getRoutes} = require('./gtfs/routes');
const {getTrips} = require('./gtfs/trips');
const {getStops, getStopsAsGeoJSON} = require('./gtfs/stops');
const {getStoptimes} = require('./gtfs/stop-times');


exports.import = importGTFS;

exports.getAgencies = getAgencies;

exports.getCalendarDates = getCalendarDates;

exports.getCalendars = getCalendars;


exports.getRoutes = getRoutes;
exports.getTrips = getTrips;

exports.getStops = getStops;
exports.getStopsAsGeoJSON = getStopsAsGeoJSON;

exports.getStoptimes = getStoptimes;



